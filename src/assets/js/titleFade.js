
const fadeTitleFunction = () => {

    var arr = Array.prototype.slice.call( children );

    const wording = ["Featuring 90% recycled materials", "Carefully-chosen sustainable materials", "Be fast on your feet and have a low impact on the planet"];


let speed = 7000;
let count = 0;
let index = 1;


changeFun();
setInterval(() => changeFun(), speed);

function changeFun() {
    
    fadeTitle.classList.add('fade');

    setTimeout(function(){

        fadeTitle.innerHTML = `<h2>${wording[index]}</h2>`;
   
        fadeTitle.classList.remove('fade');
    index++;

        if(index > wording.length - 1){
            index = 0;
        }

    }, 500);
}


}

const fadeTitle = document.querySelector('#fadeTitle');
const children = fadeTitle.children;

export default fadeTitleFunction;